import 'package:flutter/material.dart';
import 'package:gigqo_assignment/models/track.dart';

class TrackList extends StatelessWidget {
  final List<Track>? trackList;
  final Function? onItemClicked;
  final Function? onItemLongClicked;

  const TrackList({this.trackList, this.onItemClicked, this.onItemLongClicked});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: trackList!.length,
        itemBuilder: (context, index) {
          Track track = trackList![index];

          return InkWell(
            onTap: () {
              if (onItemClicked != null) onItemClicked!(track.id);
            },
            onLongPress: () {
              if (onItemLongClicked != null) onItemLongClicked!(track);
            },
            child: Card(
              child: Container(
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Image.network(track.cover!, width: 80, height: 80),
                        SizedBox(width: 16),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              track.name!,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 20),
                            Text(track.artist! + ' - ' + track.year!),
                          ],
                        )
                      ],
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 16),
                        child: Icon(Icons.favorite_outline))
                  ],
                ),
              ),
            ),
          );
        });
  }
}
