import 'package:flutter/material.dart';
import 'package:gigqo_assignment/app_router.dart';
import 'package:gigqo_assignment/enum.dart';
import 'package:gigqo_assignment/models/manage_track_arg.dart';
import 'package:gigqo_assignment/models/track.dart';
import 'package:gigqo_assignment/providers/counter.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Consumer<CounterProvider>(builder: (context, counter, _) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Home'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'You have pushed the button this many times:',
              ),
              Text(
                '${counter.countNum}',
                style: Theme.of(context).textTheme.headline4,
              ),
              SizedBox(height: 20),
              TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, AppRouter.trackListScreen);
                  },
                  child: Text('Go to TrackList Screen'))
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            counter.increment();
          },
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ), // This trailing comma makes auto-formatting nicer for build methods.
      );
    });
  }
}
