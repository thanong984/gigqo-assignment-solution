import 'package:flutter/material.dart';
import '../app_router.dart';

class AppSplashScreen extends StatefulWidget {
  final String navigateTo;
  final Image? image;
  final Color backgroundColor;

  AppSplashScreen({
    required this.navigateTo,
    this.image,
    required this.backgroundColor,
  });

  @override
  _AppSplashScreenState createState() => _AppSplashScreenState();
}

class _AppSplashScreenState extends State<AppSplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 2000), () {
      Navigator.pushReplacementNamed(context, AppRouter.homePage);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.backgroundColor,
      child: Center(
        child: widget.image,
      ),
    );
  }
}
