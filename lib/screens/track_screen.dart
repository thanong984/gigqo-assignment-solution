import 'package:flutter/material.dart';
import 'package:gigqo_assignment/models/response.dart';
import 'package:gigqo_assignment/models/track.dart';
import 'package:gigqo_assignment/providers/counter.dart';
import 'package:gigqo_assignment/services/track_service.dart';
import 'package:gigqo_assignment/widgets/loading.dart';
import 'package:gigqo_assignment/widgets/something_wrong.dart';
import 'package:provider/provider.dart';

class TrackScreen extends StatefulWidget {
  final String _trackId;
  const TrackScreen(this._trackId);

  @override
  _TrackScreenState createState() => _TrackScreenState();
}

class _TrackScreenState extends State<TrackScreen> {
  TrackService _trackService = TrackService();

  void reloadData() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Track'),
        ),
        body: FutureBuilder<ApiResponse>(
            future: _trackService.fetchTrack(widget._trackId),
            builder: (context, AsyncSnapshot<ApiResponse> response) {
              switch (response.connectionState) {
                case ConnectionState.waiting:
                  return Loading();
                default:
                  if (response.hasError) {
                    return SomethingWrong(
                        message: '${response.error}', tryAgainCall: reloadData);
                  } else {
                    if (response.data!.status == 'success') {
                      Track track = response.data!.data;
                      return Container(
                        padding: EdgeInsets.only(top: 50),
                        alignment: Alignment.center,
                        child: Column(
                          children: [
                            Image.network(
                              '${track.cover}',
                            ),
                            Container(
                                margin: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  '${track.name}',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                )),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Artist: ${track.artist}'),
                                Text('Year: ${track.year}'),
                              ],
                            ),
                            SizedBox(height: 50),
                            Text(
                              'Current counter number:',
                            ),
                            Text(
                              '${context.watch<CounterProvider>().countNum}',
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            TextButton(
                                onPressed: () {
                                  context.read<CounterProvider>().decrease();
                                },
                                child: Text(
                                  'decrease',
                                  style: TextStyle(
                                      color: Theme.of(context).accentColor),
                                ))
                          ],
                        ),
                      );
                    }
                  }
                  return SomethingWrong(
                      message: response.data!.message,
                      tryAgainCall: reloadData);
              }
            }));
  }
}
