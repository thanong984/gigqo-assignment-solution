import 'package:flutter/material.dart';
import 'package:gigqo_assignment/hex_color.dart';
import 'package:gigqo_assignment/screens/splash_screen.dart';
import '../app_router.dart';

class RootScreen extends StatelessWidget {
  const RootScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppSplashScreen(
        navigateTo: AppRouter.homePage,
        image: Image.asset('images/gigo_logo.jpeg'),
        backgroundColor: HexColor('#121219'));
  }
}
