import 'package:flutter/material.dart';
import 'package:gigqo_assignment/enum.dart';
import 'package:gigqo_assignment/models/manage_track_arg.dart';
import 'package:gigqo_assignment/models/response.dart';
import 'package:gigqo_assignment/models/track.dart';
import 'package:gigqo_assignment/services/track_service.dart';

class ManageTrackViewModel extends ChangeNotifier {
  TrackService _trackService = TrackService();
  ApiResponse _result = ApiResponse();
  bool _loading = false;
  bool _hasError = false;
  ManageTrackArg? arg;
  Track? _track;

  ManageTrackViewModel({this.arg}) {
    if (arg!.actionType == ActionType.EDIT_TRACK && arg?.track != null) {
      _track = arg!.track!;
    } else {
      _track = Track();
    }
  }

  Track get updateTrack => _track!;

  ApiResponse get result => _result;

  ActionType get action => arg!.actionType!;

  bool get loading => _loading;

  bool get hasError => _hasError;

  void setName(name) {
    _track?.name = name;
  }

  void setArtist(artist) {
    _track?.artist = artist;
  }

  void setYear(year) {
    _track?.year = year;
  }

  void onSubmit() async {
    _loading = true;
    notifyListeners();

    if (arg?.actionType == ActionType.ADD_NEW_TRACK) {
      _result = await _trackService.addNewTrack(_track!);
      if (_result.status == null || _result.status == "error") {
        _hasError = true;
      }
    }

    if (arg?.actionType == ActionType.EDIT_TRACK) {
      _result = await _trackService.updateTrack(_track);
      if (_result.status == null || _result.status == "error") {
        _hasError = true;
      }
    }

    _loading = false;
    notifyListeners();
  }

  void clearError() {
    _hasError = false;
    _result = ApiResponse();
    notifyListeners();
  }

  void setCover(coverPath) {
    _track?.cover = coverPath;
  }
}
