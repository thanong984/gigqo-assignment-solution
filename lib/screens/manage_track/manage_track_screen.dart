import 'package:flutter/material.dart';
import 'package:gigqo_assignment/enum.dart';
import 'package:gigqo_assignment/models/manage_track_arg.dart';
import 'package:gigqo_assignment/screens/manage_track/manage_track_view_model.dart';
import 'package:gigqo_assignment/screens/manage_track/widgets/adding_form.dart';
import 'package:gigqo_assignment/screens/manage_track/widgets/cover_photo_upload.dart';
import 'package:gigqo_assignment/screens/manage_track/widgets/error_view.dart';
import 'package:gigqo_assignment/screens/manage_track/widgets/successful_view.dart';
import 'package:gigqo_assignment/widgets/loading.dart';
import 'package:provider/provider.dart';

class ManageTrackScreen extends StatelessWidget {
  final ManageTrackArg arg;

  ManageTrackScreen(this.arg);

  @override
  Widget build(BuildContext context) {
    final successText = arg.actionType == ActionType.ADD_NEW_TRACK
        ? 'Track added'
        : 'Track updated';
    final title = arg.actionType == ActionType.ADD_NEW_TRACK
        ? 'Add a new track'
        : 'Update a track';

    return Scaffold(
      appBar: AppBar(),
      body: ChangeNotifierProvider<ManageTrackViewModel>(
        create: (_) => ManageTrackViewModel(arg: arg),
        child: Container(
          color: Theme.of(context).primaryColor,
          child: Consumer<ManageTrackViewModel>(builder: (context, model, _) {
            if (model.loading) return Loading();
            if (model.hasError) return ErrorView('There was an error!');
            if (model.result.status == 'success') {
              return SuccessfulView('$successText successful');
            }

            if (arg.actionType == ActionType.ADD_NEW_TRACK) {
              return Column(
                children: [
                  AddingForm(title),
                  CoverPhotoUpload(),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: Center(
                      child: TextButton(
                          onPressed: () {
                            context.read<ManageTrackViewModel>().onSubmit();
                          },
                          child: Text('Add',
                              style: TextStyle(
                                  color: Theme.of(context).accentColor))),
                    ),
                  ),
                ],
              );
            }

            return Column(
              children: [
                AddingForm(title),
                CoverPhotoUpload(),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Center(
                    child: TextButton(
                        onPressed: () {
                          context.read<ManageTrackViewModel>().onSubmit();
                        },
                        child: Text('Update',
                            style: TextStyle(
                                color: Theme.of(context).accentColor))),
                  ),
                ),
              ],
            );
          }),
        ),
      ),
    );
  }
}
