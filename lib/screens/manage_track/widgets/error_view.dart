import 'package:flutter/material.dart';
import 'package:gigqo_assignment/enum.dart';
import 'package:provider/provider.dart';
import '../manage_track_view_model.dart';

class ErrorView extends StatelessWidget {
  final String message;

  ErrorView(this.message);

  @override
  Widget build(BuildContext context) {
    final model = context.read<ManageTrackViewModel>();
    final btnText =
        model.action == ActionType.ADD_NEW_TRACK ? 'Try again' : 'Back';

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.all(16),
          alignment: Alignment.center,
          child: Text(message, style: TextStyle(color: Colors.white)),
        ),
        TextButton(
            onPressed: () {
              model.clearError();
              if (model.action == ActionType.EDIT_TRACK) Navigator.pop(context);
            },
            child: Text(btnText))
      ],
    );
  }
}
