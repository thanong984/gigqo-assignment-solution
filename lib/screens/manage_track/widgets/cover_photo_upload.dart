import 'dart:io';
import 'package:flutter/material.dart';
import 'package:gigqo_assignment/screens/manage_track/manage_track_view_model.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class CoverPhotoUpload extends StatefulWidget {
  @override
  _CoverPhotoUploadState createState() => _CoverPhotoUploadState();
}

class _CoverPhotoUploadState extends State<CoverPhotoUpload> {
  final ImagePicker _picker = ImagePicker();
  XFile? _imageFile;

  void _selectPhoto(ManageTrackViewModel model) async {
    try {
      final pickedFile = await _picker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 50,
      );
      setState(() {
        _imageFile = pickedFile;
        model.setCover(pickedFile!.path);
      });
    } catch (e) {}
  }

  Widget _previewImage(ManageTrackViewModel model) {
    if (_imageFile != null) {
      return Image.file(File(_imageFile!.path), width: 200, height: 150);
    }
    if (model.updateTrack.cover != null) {
      return Image.network(model.updateTrack.cover!, width: 200, height: 150);
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    final model = context.read<ManageTrackViewModel>();
    return Column(
      children: [
        Container(
          child: TextButton(
              onPressed: () {
                _selectPhoto(model);
              },
              child: Text('Select photo cover')),
        ),
        _previewImage(model)
      ],
    );
  }
}
