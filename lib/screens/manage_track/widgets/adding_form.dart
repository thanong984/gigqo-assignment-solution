import 'package:flutter/material.dart';
import 'package:gigqo_assignment/models/track.dart';
import 'package:provider/provider.dart';
import '../../../enum.dart';
import '../manage_track_view_model.dart';

class AddingForm extends StatelessWidget {
  final String title;

  AddingForm(this.title);

  @override
  Widget build(BuildContext context) {
    final model = context.read<ManageTrackViewModel>();
    Track? track;
    if (model.action == ActionType.EDIT_TRACK) {
      track = model.updateTrack;
    }

    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(16),
          child: Text(title,
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Colors.white)),
        ),
        Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
          child: TextFormField(
            initialValue: track?.name,
            onChanged: (value) => model.setName(value),
            decoration: InputDecoration(
                fillColor: Colors.white,
                filled: true,
                border: InputBorder.none,
                hintText: 'Name'),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
          child: TextFormField(
            initialValue: track?.artist,
            onChanged: (value) => model.setArtist(value),
            decoration: InputDecoration(
                fillColor: Colors.white,
                filled: true,
                border: InputBorder.none,
                hintText: 'Artist '),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
          child: TextFormField(
            initialValue: track?.year,
            onChanged: (value) => model.setYear(value),
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                fillColor: Colors.white,
                filled: true,
                border: InputBorder.none,
                hintText: 'Year '),
          ),
        ),
      ],
    );
  }
}
