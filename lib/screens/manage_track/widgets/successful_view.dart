import 'package:flutter/material.dart';

class SuccessfulView extends StatelessWidget {
  final String message;

  SuccessfulView(this.message);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.all(16),
          alignment: Alignment.center,
          child: Text(message, style: TextStyle(color: Colors.white)),
        ),
        TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text('OK'))
      ],
    );
  }
}
