import 'package:flutter/material.dart';
import 'package:gigqo_assignment/app_router.dart';
import 'package:gigqo_assignment/models/manage_track_arg.dart';
import 'package:gigqo_assignment/models/response.dart';
import 'package:gigqo_assignment/models/track.dart';
import 'package:gigqo_assignment/services/track_service.dart';
import 'package:gigqo_assignment/widgets/loading.dart';
import 'package:gigqo_assignment/widgets/something_wrong.dart';
import 'package:gigqo_assignment/widgets/track_list.dart';

import '../enum.dart';

class TrackListScreen extends StatefulWidget {
  @override
  _TrackListScreenState createState() => _TrackListScreenState();
}

class _TrackListScreenState extends State<TrackListScreen> {
  final _trackService = TrackService();

  void reloadData() {
    setState(() {});
  }

  void onListItemClicked(String trackId) {
    Navigator.pushNamed(context, AppRouter.trackScreen, arguments: trackId);
  }

  void onListItemLongClicked(Track track) {
    _showEditDialog(track);
  }

  Future<void> _showEditDialog(Track track) async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Edit Track'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Are you want to edit the track?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Yes'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.pushNamed(context, AppRouter.addNewTrackScreen,
                    arguments: ManageTrackArg(
                        actionType: ActionType.EDIT_TRACK, track: track));
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Track List'),
      ),
      body: FutureBuilder<ApiResponse>(
          future: _trackService.fetchAllTracks(),
          builder: (context, AsyncSnapshot<ApiResponse> response) {
            switch (response.connectionState) {
              case ConnectionState.waiting:
                return Loading();
              default:
                if (response.hasError) {
                  return SomethingWrong(
                      message: 'Error fetch tracks data',
                      tryAgainCall: reloadData);
                } else {
                  if (response.data!.status == 'success')
                    return ListView(
                      children: [
                        TextButton(
                            onPressed: () {
                              Navigator.pushNamed(
                                  context, AppRouter.addNewTrackScreen,
                                  arguments: ManageTrackArg(
                                    actionType: ActionType.ADD_NEW_TRACK,
                                  ));
                            },
                            child: Text('Add a new track')),
                        TrackList(
                          trackList: response.data!.data!,
                          onItemClicked: onListItemClicked,
                          onItemLongClicked: onListItemLongClicked,
                        )
                      ],
                    );
                }
                return SomethingWrong(
                    message: response.data!.message, tryAgainCall: reloadData);
            }
          }),
    );
  }
}
