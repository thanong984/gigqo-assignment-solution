import 'dart:convert';

import 'package:gigqo_assignment/models/response.dart';
import 'package:gigqo_assignment/models/track.dart';
import 'package:gigqo_assignment/web_api.dart';
import 'package:http/http.dart' as http;

class TrackService {
  WebApi _webApi = WebApi();

  Future<ApiResponse> fetchAllTracks() async {
    ApiResponse response = ApiResponse();

    try {
      http.Response result = await _webApi.getTracks();
      final json = jsonDecode(result.body);

      if (json['status'] == 'success') {
        response = ApiResponse.fromJson(json);
        List<Track> tracks =
            List<Track>.from(json['data'].map((x) => Track.fromJson(x)));
        response.data = tracks;
      }
      response.status = json['status'];
      response.message = json['message'];
    } catch (e) {
      print(e);
    }

    return response;
  }

  Future<ApiResponse> fetchTrack(String id) async {
    ApiResponse response = ApiResponse();
    http.Response result = await _webApi.getTrackById(id);
    final json = jsonDecode(result.body);

    try {
      if (json['status'] == 'success') {
        response = ApiResponse.fromJson(json);
        Track track = Track.fromJson(json['data']);
        response.data = track;
      }
      response.status = json['status'];
      response.message = json['message'];
    } catch (e) {
      print(e);
    }

    return response;
  }

  Future<ApiResponse> addNewTrack(Track track) async {
    ApiResponse response = ApiResponse();
    http.Response result = await _webApi.postTracks(track);
    final json = jsonDecode(result.body);

    try {
      if (json['status'] == 'success') {
        Track track = Track.fromJson(json['data']);
        response.data = track;
      }
      response.status = json['status'];
      response.message = json['message'];
    } catch (e) {
      print(e);
    }

    return response;
  }

  Future<ApiResponse> updateTrack(Track? track) async {
    ApiResponse response = ApiResponse();
    http.Response result = await _webApi.putTrack(track);
    final json = jsonDecode(result.body);

    try {
      response.status = json['status'];
      if (json['status'] == 'success') {
        Track track = Track.fromJson(json['data']);
        response.data = track;
      }
      response.status = json['status'];
      response.message = json['message'];
    } catch (e) {
      print(e);
    }

    return response;
  }
}
