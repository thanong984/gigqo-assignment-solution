import 'package:flutter/material.dart';
import 'package:gigqo_assignment/models/manage_track_arg.dart';
import 'package:gigqo_assignment/screens/home_screen.dart';
import 'package:gigqo_assignment/screens/manage_track/manage_track_screen.dart';
import 'package:gigqo_assignment/screens/root_screen.dart';
import 'package:gigqo_assignment/screens/track_list_screen.dart';
import 'package:gigqo_assignment/screens/track_screen.dart';

class AppRouter {
  static const String initiationRoute = '/';
  static const String homePage = 'HOME';
  static const String trackListScreen = 'TRACK_LIST_SCREEN';
  static const String trackScreen = 'TRACK_SREEN';
  static const String addNewTrackScreen = 'ADD_NEW_TRACK_SCREEN';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case initiationRoute:
        return MaterialPageRoute(builder: (_) => RootScreen());
      case homePage:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case trackListScreen:
        return MaterialPageRoute(builder: (_) => TrackListScreen());
      case trackScreen:
        return MaterialPageRoute(
            builder: (_) => TrackScreen(settings.arguments as String));
      case addNewTrackScreen:
        return MaterialPageRoute(
            builder: (_) =>
                ManageTrackScreen(settings.arguments as ManageTrackArg));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No screen you requested!'),
                  ),
                ));
    }
  }
}
