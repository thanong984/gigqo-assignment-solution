import 'package:flutter/material.dart';
import 'package:gigqo_assignment/providers/counter.dart';
import 'package:provider/provider.dart';
import 'app_router.dart';
import 'hex_color.dart';

void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (_) => CounterProvider()),
    // ChangeNotifierProvider(create: (_) => AddNewTrackViewModel())
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: HexColor('#121219'),
        accentColor: HexColor('##DE0E22'),
      ),
      onGenerateRoute: AppRouter.generateRoute,
      initialRoute: AppRouter.initiationRoute,
    );
  }
}
