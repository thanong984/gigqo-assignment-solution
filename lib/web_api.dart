import 'dart:convert';
import 'dart:io';

import 'package:gigqo_assignment/models/track.dart';
import 'package:http/http.dart' as http;

class WebApi {
  final _baseUrl = 'https://beta-api.gigqo.com/api';

  Future<http.Response> getTracks() async {
    final response = await http.get(
      Uri.parse(_baseUrl + '/mockup/tracks'),
    );
    return response;
  }

  Future<http.Response> getTrackById(String id) async {
    final response = await http.get(
      Uri.parse(_baseUrl + '/mockup/tracks/$id'),
    );
    return response;
  }

  Future<http.Response> postTracks(Track track) async {
    final response = await http.post(Uri.parse(_baseUrl + '/mockup/tracks'),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
        body: jsonEncode(track.toJson()));
    return response;
  }

  Future<http.Response> putTrack(Track? track) async {
    return await http.post(Uri.parse(_baseUrl + '/mockup/tracks'),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
        body: jsonEncode(track!.toJson()));
  }
}
