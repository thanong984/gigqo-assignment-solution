import 'dart:convert';

ApiResponse orderItemFromJson(String str) =>
    ApiResponse.fromJson(json.decode(str));

class ApiResponse {
  ApiResponse({
    this.status,
    this.data,
    this.message,
  });

  String? status;
  dynamic data;
  String? message;

  factory ApiResponse.fromJson(Map<String, dynamic> json) => ApiResponse(
        status: json['status'],
        data: json['data'],
        message: json['message'],
      );
}
