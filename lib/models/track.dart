import 'dart:convert';

Track trackFromJson(String str) => Track.fromJson(json.decode(str));

String trackToJson(Track data) => json.encode(data.toJson());

class Track {
  Track({
    this.id,
    this.name,
    this.artist,
    this.year,
    this.cover,
  });

  String? id;
  String? name;
  String? artist;
  String? year;
  String? cover;

  factory Track.fromJson(Map<String, dynamic> json) => Track(
        id: json["id"],
        name: json["name"],
        artist: json["artist"],
        year: json["year"],
        cover: json["cover"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "artist": artist,
        "year": year,
        "cover": cover,
      };
}
