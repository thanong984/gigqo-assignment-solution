import 'package:gigqo_assignment/enum.dart';
import 'package:gigqo_assignment/models/track.dart';

class ManageTrackArg {
  Track? track;
  ActionType? actionType;

  ManageTrackArg({this.track, this.actionType});
}
